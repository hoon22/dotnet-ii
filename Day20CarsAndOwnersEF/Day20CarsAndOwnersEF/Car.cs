﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    public class Car
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string MakeModel { get; set; }   // up to 100 characters
        public int YearOfProd { get; set; }

        // Foreign key for Owner
        public virtual Owner Owner { get; set; }        // relation, make nullable

        public override string ToString()
        {
            return string.Format("[{0}] {1}, {2}", Id, MakeModel, YearOfProd);
        }
    }
}
