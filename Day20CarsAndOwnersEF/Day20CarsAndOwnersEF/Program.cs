﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    class Program
    {
        static readonly ParkingContext ctx = new ParkingContext();

        // Display user command menu
        static void DisplayMenu()
        {
            Console.WriteLine(
                "\n1. List all cars and their owner\n" +
                "2. List all owners and their cars\n" +
                "3. Add a car (no owner)\n" +
                "4. Add an owner (no cars)\n" +
                "5. Assign car to a new owner (or no owner)\n" +
                "6. Delete an owner with all cars they own\n" +
                "0. Quit"
            );
            Console.Write("Choice: ");
        }

        // if command is invalid, return -1. otherwise return value 0 - 4
        static int GetUserCommand()
        {
            DisplayMenu();

            if (!int.TryParse(Console.ReadLine(), out int command))
            {
                return -1;  // error
            }
            return command;
        }

        static void GetAllCarsAndTheirOwner()
        {
            if (ctx.CarCollection.Count() == 0)
            {
                Console.WriteLine("No car in database");
                return;
            }

            foreach (Car car in ctx.CarCollection)
            {
                Console.WriteLine("[{0}]:{1},{2} => {3}", car.Id, car.MakeModel, car.YearOfProd, car.Owner == null ? "No owner" : car.Owner.ToString());
            }
        }

        static void GetAllOwnerAndTheirCars()
        {
            if (ctx.OwnerCollection.Count() == 0)
            {
                Console.WriteLine("No owner in database");
                return;
            }

            // Eager Loading...
            // ctx.OwnerCollection.Include("CarsCollection").ToList();

            foreach (Owner owner in ctx.OwnerCollection)
            {
                Console.WriteLine("[{0}]:{1} have(has) {2} car(s)", owner.Id, owner.Name, owner.CarsCollection.Count());
                foreach (Car car in owner.CarsCollection.ToList())
                {
                    Console.WriteLine(" -> {0}", car);
                }
            }
        }

        static void AddCar()
        {
            Console.Write("Car MakeModel: ");
            string carMake = Console.ReadLine();
            if (carMake == "")
            {
                Console.WriteLine("[ERROR] Please enter car make model");
                return;
            }

            Console.Write("Year of production: ");
            if (!int.TryParse(Console.ReadLine(), out int year))
            {
                Console.WriteLine("[ERROR] Invalid year");
                return;
            }
            
            // Add to database
            ctx.CarCollection.Add(new Car() { MakeModel = carMake, YearOfProd = year });
            ctx.SaveChanges();
        }

        static void AddOwner()
        {
            Console.Write("Owner Name: ");
            string name = Console.ReadLine();
            if (name == "")
            {
                Console.WriteLine("[ERROR] Please enter owner name");
                return;
            }

            // Add to database
            ctx.OwnerCollection.Add(new Owner() { Name = name });
            ctx.SaveChanges();
        }

        static void AssignCarToNewOwner()
        {
            Console.WriteLine("------- Car List -------");
            foreach (Car car in ctx.CarCollection)
            {
                Console.WriteLine("{0}: {1}, {2}", car.Id, car.MakeModel, car.YearOfProd);
            }

            Console.Write("Enter Car Id: ");
            if (!int.TryParse(Console.ReadLine(), out int carId))
            {
                Console.WriteLine("[ERROR] Invalid Car ID");
                return;
            }

            Console.WriteLine("\n------- Owner List -------");
            foreach (Owner owner in ctx.OwnerCollection)
            {
                Console.WriteLine("{0}: {1}", owner.Id, owner.Name);
            }

            Console.Write("Enter New Owner Id (0 to reset owner): ");
            if (!int.TryParse(Console.ReadLine(), out int ownerId))
            {
                Console.WriteLine("[ERROR] Invalid Owner Id");
                return;
            }

            Car selectedCar = ctx.CarCollection.Where(car => car.Id == carId).SingleOrDefault();
            // check car id is valid
            if (selectedCar == null)
            {
                Console.WriteLine("[ERROR] Car Id {0} is not valid", carId);
                return;
            }

            Owner newOwner = ctx.OwnerCollection.Where(owner => owner.Id == ownerId).SingleOrDefault();
            // check user id is valid
            if (newOwner == null && ownerId != 0)
            {
                Console.WriteLine("[ERROR] Owner Id {0} is not valid", ownerId);
                return;
            }

            selectedCar.Owner = newOwner;
            ctx.SaveChanges();
        }


        static void DeleteOwner()
        {
            Console.WriteLine("\n------- Owner List -------");
            foreach (Owner owner in ctx.OwnerCollection)
            {
                Console.WriteLine("{0}: {1}", owner.Id, owner.Name);
            }

            Console.Write("Enter Owner Id to delete: ");
            if (!int.TryParse(Console.ReadLine(), out int ownerId))
            {
                Console.WriteLine("[ERROR] Invalid id type");
                return;
            }

            Owner deletingOwner = ctx.OwnerCollection.Where(owner => owner.Id == ownerId).SingleOrDefault();
            // check user id is valid
            if (deletingOwner == null)
            {
                Console.WriteLine("[ERROR] Owner Id {0} is not valid", ownerId);
                return;
            }

            // delete car
            // ctx.CarCollection.RemoveRange(deletingOwner.CarsCollection);
            // delete owner
            ctx.OwnerCollection.Remove(deletingOwner);
            ctx.SaveChanges();
        }

        static void Main(string[] args)
        {
            try
            {
                int command;
                do
                {
                    command = GetUserCommand();
                    try
                    {
                        switch (command)
                        {
                            case 0:
                                break;
                            case 1:
                                GetAllCarsAndTheirOwner();
                                break;
                            case 2:
                                GetAllOwnerAndTheirCars();
                                break;
                            case 3:
                                AddCar();
                                break;
                            case 4:
                                AddOwner();
                                break;
                            case 5:
                                AssignCarToNewOwner();
                                break;
                            case 6:
                                DeleteOwner();
                                break;
                            default:
                                Console.WriteLine("Invalid command try again.\n");
                                break;
                        }
                    }
                    catch (DataException ex)
                    {
                        Console.WriteLine("[ERROR] " + ex.Message);
                    }
                } while (command != 0);

                Console.WriteLine("\nGood bye!\n");
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
