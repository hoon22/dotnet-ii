﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    public class Owner
    {
        public Owner()
        {
            this.CarsCollection = new HashSet<Car>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }   // up to 100 characters

        public virtual ICollection<Car> CarsCollection { get; set; } // relation

        public override string ToString()
        {
            return string.Format("[{0}] {1}", Id, Name);
        }
    }
}
