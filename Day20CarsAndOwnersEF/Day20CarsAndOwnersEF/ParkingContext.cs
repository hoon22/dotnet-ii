﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    public class ParkingContext : DbContext
    {
        public ParkingContext() : base()
        {

        }

        public DbSet<Owner> OwnerCollection { get; set; }
        public DbSet<Car> CarCollection { get; set; }
    }
}
