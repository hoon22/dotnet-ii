﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21Delegate
{
    class Program
    {
        // step 1: declare delegate type
        public delegate void LogMessageDelegate(string s);

        // step 2: declare a field of delegate type
        static LogMessageDelegate Logger;   // initially null when empty

        // step 3: create one or more methods whose signatures match that of delegate
        static void LogToConsole(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }

        static void LogToFile(string msg)
        {
            File.AppendAllText(@"test.log", "MSG: " + msg);
        }

        static void LogToConsoleFancy(string msg)
        {
            Console.WriteLine("**************** : " + msg);
        }

        static void Main(string[] args)
        {
            try
            {
                Random rand = new Random();

                if (rand.Next(2) != 0) Logger += LogToConsole;
                if (rand.Next(2) != 0) Logger += LogToFile;
                if (rand.Next(2) != 0) Logger += LogToConsoleFancy;
                //
                Logger("This is first");
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
