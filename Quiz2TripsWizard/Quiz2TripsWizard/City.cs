﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    public class City
    {
        public int Id;
        public string CityName;
        public double Latitude;
        public double Longitude;

        public override string ToString()
        {
            return CityName;
        }
    }
}
