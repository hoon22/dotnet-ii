﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<TripWithCities> TripList = new List<TripWithCities>();

        public MainWindow()
        {
            InitializeComponent();
            lvTrips.ItemsSource = TripList;

            try
            {
                Globals.Db = new Database();
                // Load data into ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message, "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        public void ReloadList()
        {
            try { 
                // Clear old ItemsList
                TripList.Clear();
                // Add new list to ItemsList
                TripList.AddRange(Globals.Db.GetAllTripsWithCities());
                lvTrips.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvTrips.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select trip list to save", "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text files|*.txt|All files|*.*";
            saveFileDialog1.Title = "Save a data file";
            saveFileDialog1.ShowDialog();

            try
            {
                if (saveFileDialog1.FileName != "")
                {
                    List<string> dataList = new List<string>();
                    foreach (TripWithCities item in lvTrips.SelectedItems)
                    {
                        dataList.Add(item.ToSaveFormat());
                    }
                    File.WriteAllLines(saveFileDialog1.FileName, dataList);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to save a file " + ex.Message, "Car List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddTrip_MenuClick(object sender, RoutedEventArgs e)
        {
            SelectCityDialog selectCityDlg = new SelectCityDialog(this);
            if (selectCityDlg.ShowDialog() == true)
            {
                AddTripDialog addTripDlg = new AddTripDialog(this);
                if (addTripDlg.ShowDialog() == true)
                {
                    ReloadList();
                }
            }
        }
    }
}
