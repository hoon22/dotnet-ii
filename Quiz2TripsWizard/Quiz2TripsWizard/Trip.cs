﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    public class Trip
    {
        public int Id;
        public string PersonName;
        public DateTime DepartureDate;
        public DateTime ReturnDate;
        public int FromCityId;
        public int ToCityId;
    }
}
