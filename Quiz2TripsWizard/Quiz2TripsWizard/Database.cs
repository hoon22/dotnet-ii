﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    public class Database
    {
        // Set DbConnectionString from DB Property
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896357\MyLabs\DotNet-2\Quiz2TripsWizard\TripDB.mdf;Integrated Security=True;Connect Timeout=30";

        private SqlConnection conn;

        // DB Constructor
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }


        /// <summary>
        /// Get list of all cities
        /// </summary>
        /// <returns>List of City</returns>
        public List<City> GetAllCities()
        {
            List<City> list = new List<City>();

            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Cities", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string cityName = (string)reader["CityName"];
                    double latitude = (double)reader["Latitude"];
                    double longitude = (double)reader["Longitude"];
                    list.Add(new City() { Id = id, CityName = cityName, Latitude = latitude, Longitude = longitude });
                }
            }
            return list;
        }

        public int AddTrip(Trip trip)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Trips (PersonName, DepartureDate, ReturnDate, FromCityId, ToCityId) OUTPUT INSERTED.ID VALUES (@PersonName, @DepartureDate, @ReturnDate, @FromCityId, @ToCityId)", conn);
            cmdInsert.Parameters.AddWithValue("PersonName", trip.PersonName);
            cmdInsert.Parameters.AddWithValue("DepartureDate", trip.DepartureDate);
            cmdInsert.Parameters.AddWithValue("ReturnDate", trip.ReturnDate);
            cmdInsert.Parameters.AddWithValue("FromCityId", trip.FromCityId);
            cmdInsert.Parameters.AddWithValue("ToCityId", trip.ToCityId);

            int insertId = (int)cmdInsert.ExecuteScalar();
            trip.Id = insertId;
            // return generated id
            return insertId;
        }

        public List<TripWithCities> GetAllTripsWithCities()
        {
            List<TripWithCities> list = new List<TripWithCities>();

            string queryStr = "SELECT T.Id as [TripId], T.PersonName, T.FromCityId, T.ToCityId, " +
                "FC.CityName as [FromCityName], TC.CityName as [ToCityName], " +
                "FC.Latitude as [FromCityLatitude], FC.Longitude as [FromCityLongitude], " +
                "TC.Latitude as [ToCityLatitude], TC.Longitude as [ToCityLongitude], " +
                "T.DepartureDate, T.ReturnDate " +
                "FROM Trips as T " +
                "inner join Cities as FC on T.FromCityId = FC.Id " +
                "inner join Cities as TC on T.ToCityId = TC.Id";

            SqlCommand cmdSelect = new SqlCommand(queryStr, conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["TripId"];
                    string personName = (string)reader["PersonName"];
                    int fromCityId = (int)reader["FromCityId"];
                    int toCityId = (int)reader["ToCityId"];
                    string fromCityName = (string)reader["FromCityName"];
                    string toCityName = (string)reader["toCityName"];
                    double fromCitylatitude = (double)reader["FromCitylatitude"];
                    double fromCitylongitude = (double)reader["FromCitylongitude"];
                    double toCitylatitude = (double)reader["ToCitylatitude"];
                    double toCitylongitude = (double)reader["ToCitylongitude"];
                    DateTime departureDate = (DateTime)reader["DepartureDate"];
                    DateTime returnDate = (DateTime)reader["ReturnDate"];
                    //
                    list.Add(new TripWithCities() {
                        TripId = id, PersonName = personName, FromCityId = fromCityId, ToCityId = toCityId,
                        FromCityName = fromCityName, FromCityLatitude = fromCitylatitude, FromCityLongitude = fromCitylongitude,
                        ToCityName = toCityName, ToCityLatitude = toCitylatitude, ToCityLongitude = toCitylongitude,
                        DepartureDate = departureDate, ReturnDate = returnDate
                    });
                }
            }
            return list;
        }
    }
}
