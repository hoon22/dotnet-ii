﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    public class TripWithCities
    {
        public int TripId { get; set; }
        public string PersonName { get; set; }
        public int FromCityId { get; set; }
        public int ToCityId { get; set; }
        public string FromCityName { get; set; }
        public string ToCityName { get; set; }
        public double FromCityLatitude { get; set; }
        public double FromCityLongitude { get; set; }
        public double ToCityLatitude { get; set; }
        public double ToCityLongitude { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public double TripDistanceKm
        {
            get
            {
                var sCoord = new GeoCoordinate(FromCityLatitude, FromCityLongitude);
                var eCoord = new GeoCoordinate(ToCityLatitude, ToCityLongitude);
                double distanceKm = sCoord.GetDistanceTo(eCoord) / 1000;

                return distanceKm;
            }
        }   // computed, not stored

        public string ToSaveFormat()
        {
            return string.Format("{0},{1},{2},{3},{4:0.000},{5:d},{6:d}", TripId, PersonName, FromCityName, ToCityName, TripDistanceKm, DepartureDate, ReturnDate);
        }
    }
}
