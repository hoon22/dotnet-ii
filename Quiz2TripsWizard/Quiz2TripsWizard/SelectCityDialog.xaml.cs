﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for SelectCityDialog.xaml
    /// </summary>
    public partial class SelectCityDialog : Window
    {
        List<City> CityList = new List<City>();

        public SelectCityDialog(Window owner)
        {
            InitializeComponent();

            Owner = owner;
            try
            {
                // Clear old ItemsList
                CityList = Globals.Db.GetAllCities();

                listBoxFromCity.ItemsSource = CityList;
                listBoxToCity.ItemsSource = CityList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ListBoxCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBoxFromCity.SelectedIndex == -1 || listBoxToCity.SelectedIndex == -1)
            {
                btNext.IsEnabled = false;
                return;
            }

            if (listBoxFromCity.SelectedIndex == listBoxToCity.SelectedIndex)
            {
                MessageBox.Show("FromCity should not be same as ToCity", "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Error);
                btNext.IsEnabled = false;
                return;
            }

            Globals.FromCity = listBoxFromCity.SelectedItem as City;
            Globals.ToCity = listBoxToCity.SelectedItem as City;

            btNext.IsEnabled = true;
        }
    }
}
