﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for AddTripDialog.xaml
    /// </summary>
    public partial class AddTripDialog : Window
    {
        public AddTripDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;

            // Set init values.
            tbFromCityName.Text = Globals.FromCity.CityName;
            tbToCityName.Text = Globals.ToCity.CityName;
        }

        private void ButtonFinish_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                DateTime departureDate = dpDepartureDate.SelectedDate.Value;
                DateTime returnDate = dpReturnDate.SelectedDate.Value;

                Trip trip = new Trip() { PersonName = name, DepartureDate = departureDate, ReturnDate = returnDate, FromCityId = Globals.FromCity.Id, ToCityId = Globals.ToCity.Id };
                Globals.Db.AddTrip(trip);

                // set dialog result
                DialogResult = true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckValidation();
        }

        private void DpDepartureDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckValidation();
        }

        private void DpReturnDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckValidation();
        }

        private void CheckValidation()
        {
            if (tbName.Text == "" || dpDepartureDate.SelectedDate == null || dpReturnDate.SelectedDate == null)
            {
                btFinish.IsEnabled = false;
                return;
            }

            // Check name is valid
            if (!Regex.IsMatch(tbName.Text, @"^[^,]{1,100}$"))
            {
                MessageBox.Show("Warning: Name should be 1-100 characters long and can't contain comma(,)", "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Warning);
                btFinish.IsEnabled = false;
                return;
            }

            if (dpDepartureDate.SelectedDate.Value >= dpReturnDate.SelectedDate.Value)
            {
                MessageBox.Show("Warning: Departure date should be before Return date", "Trip with Cities", MessageBoxButton.OK, MessageBoxImage.Warning);
                btFinish.IsEnabled = false;
                return;
            }

            btFinish.IsEnabled = true;
        }
    }
}
