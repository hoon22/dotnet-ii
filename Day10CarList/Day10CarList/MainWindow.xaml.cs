﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10CarList
{
/*
    public enum CarMake
    {
        Audi,
        BMW,
        GM,
        Honda,
        Hyundai,
        Toyota
    }
*/
    public class Car
    {
        public string Make;
        public string CarSize;      // FIXME: change into Enum
        public string Features;     // FIXME: directory of Enum or string
        public string Plates;
        public double WeightTonnes;

        public override string ToString()
        {
            return string.Format("{0} is {1} with {2} reg {3}, {4:0.00}t", Make, CarSize, Features, Plates, WeightTonnes);
        }

        public string ConvertToSaveFormat()
        {
            return string.Format("{0};{1};{2};{3};{4:0.00}", Make, CarSize, Features, Plates, WeightTonnes);
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carList = new List<Car>();
        bool hasUnsavedData = false;

        public MainWindow()
        {
            InitializeComponent();
            /*
            carList.Add(new Car() { Make = "Audi", CarSize = "Compact", Features = "", Plates = "AZL867", WeightTonnes = 1.23 });
            carList.Add(new Car() { Make = "Honda", CarSize = "SUV", Features = "", Plates = "LKM230", WeightTonnes = 1.59 });
            carList.Add(new Car() { Make = "Hyundai", CarSize = "Van", Features = "", Plates = "FCL126", WeightTonnes = 2.06 });
            */
            lvCars.ItemsSource = carList;

            List<string> carMakeList = new List<string>();
            carMakeList.Add("Audi");
            carMakeList.Add("BMW");
            carMakeList.Add("GM");
            carMakeList.Add("Honda");
            carMakeList.Add("Hyundai");
            carMakeList.Add("Toyota");
            cmbCarMake.ItemsSource = carMakeList;

            LoadFromFile("default.dat");
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select car list to save", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text files|*.txt|All files|*.*";
            saveFileDialog1.Title = "Save a data file";
            saveFileDialog1.ShowDialog();

            try
            {
                if (saveFileDialog1.FileName != "")
                {
                    List<string> dataList = new List<string>();
                    foreach (Car car in lvCars.SelectedItems)
                    {
                        dataList.Add(car.ConvertToSaveFormat());
                    }
                    File.WriteAllLines(saveFileDialog1.FileName, dataList);
                    // update save flag
                    hasUnsavedData = false;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to save a file " + ex.Message, "Car List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonSaveAndClear_Click(object sender, RoutedEventArgs e)
        {
            if (cmbCarMake.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Car Make", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            if (!Regex.IsMatch(tbPlates.Text, @"^[A-Z0-9]{3,10}$"))
            {
                MessageBox.Show("[ERROR] Invalid Car Plate Number", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            Car newCar = new Car();

            newCar.Make = cmbCarMake.SelectedValue.ToString();

            if (rbCarSizeCompact.IsChecked == true)
            {
                newCar.CarSize = "Compact";
            }
            else if (rbCarSizeVan.IsChecked == true)
            {
                newCar.CarSize = "Van";
            }
            else if (rbCarSizeSUV.IsChecked == true)
            {
                newCar.CarSize = "SUV";
            } else
            {
                MessageBox.Show("[ERROR] Internal system error", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            // checkbox
            List<string> featureList = new List<string>();
            if (cbFeaturesABS.IsChecked == true) featureList.Add("ABS");
            if (cbFeaturesBluetooth.IsChecked == true) featureList.Add("Bluetooth");
            if (cbFeaturesOther.IsChecked == true) featureList.Add("Other");

            newCar.Features = string.Join(",", featureList);

            newCar.Plates = tbPlates.Text;
            newCar.WeightTonnes = Math.Round(slWeightTonnes.Value, 2, MidpointRounding.AwayFromZero);

            carList.Add(newCar);
            hasUnsavedData = true;
            // need to refresh
            lvCars.Items.Refresh();

            // cleanup
            cmbCarMake.SelectedIndex = -1;
            rbCarSizeCompact.IsChecked = true;

            cbFeaturesABS.IsChecked = false;
            cbFeaturesBluetooth.IsChecked = false;
            cbFeaturesOther.IsChecked = false;

            slWeightTonnes.Value = 0;
            tbPlates.Text = "";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hasUnsavedData)
            {
                string message = string.Format("Are you sure to exit?\n{0}", hasUnsavedData ? "You have unsaved data" : "");
                MessageBoxResult result = MessageBox.Show(message, "Car List", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);

                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void ImportFromFile_MenuClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Car list|*.txt|All Files|*.*"; // Filter files by extension

            if (dlg.ShowDialog() == true)
            {
                LoadFromFile(dlg.FileName);
            }
        }

        // method to load file
        void LoadFromFile(string fileName) {
            try
            {
                string[] dataList = File.ReadAllLines(fileName);

                foreach (string line in dataList)
                {
                    // create new Car Object
                    // 1. parsing data
                    // Toyota;SUV;;LKJ76D;1.39
                    string[] data = line.Split(';');
                    string make = data[0];
                    string size = data[1];
                    string features = data[2];
                    string plates = data[3];
                    double weightTonnes;

                    if (!double.TryParse(data[4], out weightTonnes))
                    {
                        MessageBox.Show("[ERROR] Invalid data type ", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                        return;
                    }
                    // 2. create new car
                    Car newCar = new Car() { Make = make, CarSize = size, Features = features, Plates = plates, WeightTonnes = weightTonnes };

                    // 3. Add new object into car list
                    carList.Add(newCar);
                }
                // update list view
                lvCars.Items.Refresh();
                lblStatusBar.Content = string.Format("Succefully read {0} records from {1}.", dataList.Length, fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to read file " + ex.Message, "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
        }

        private void LvCars_Selected(object sender, RoutedEventArgs e)
        {
            Car car = lvCars.SelectedItem as Car;

            cmbCarMake.SelectedValue = car.Make;
            tbPlates.Text = car.Plates;
            slWeightTonnes.Value = car.WeightTonnes;

            lblStatusBar.Content = string.Format("Selected from the list: {0}", car);
        }
    }
}
