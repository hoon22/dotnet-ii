﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Airport
    {
        public Airport()
        {
            FlightsList = new HashSet<Flight>();
        }

        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(3)]
        public string Code { get; set; } // 3 uppercase letters

        [MaxLength(50)]
        public string City { get; set; } // 0-50 letters

        public virtual ICollection<Flight> FlightsList { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] {1}, {2}", Id, Code, City);
        }
    }
}
