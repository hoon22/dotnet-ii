﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    class Program
    {
        static readonly AirTravelContext2 ctx = new AirTravelContext2();

        static readonly SuperFib superFib = new SuperFib();

        public delegate void LogMessageDelegate(string s);
        static LogMessageDelegate Logger;

        public static void LogToScreen(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void LogToFile(string msg)
        {
            try
            {
                File.AppendAllText(@"..\..\..\main.log", string.Format("{0} {1}{2}", DateTime.Now, msg, Environment.NewLine));
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Can't write log file: " + ex.Message);
            }
        }

        public static void LogToDatabase(string msg)
        {
            ctx.LogMsgCollection.Add(new LogMessage(msg));
            ctx.SaveChanges();
        }

        // Display user command menu
        static void DisplayMenu()
        {
            Console.WriteLine(
                "\n1. Add airport\n" +
                "2. Register a flight from/to airports\n" +
                "3. Show all flights involving a specific airport (from or to) by airport code\n" +
                "4. Delete an airport and all flights associated with it\n" +
                "5. Display Nth SuperFib number\n" +
                "6. Change exception handlers delegates\n" +
                "0. Exit"
            );
            Console.Write("Choice: ");
        }

        // if command is invalid, return -1. otherwise return value 0 - 4
        static int GetUserCommand()
        {
            DisplayMenu();

            if (!int.TryParse(Console.ReadLine(), out int command))
            {
                return -1;  // error
            }
            return command;
        }

        static void AddAirport()
        {
            Console.Write("Airport Code: ");
            string code = Console.ReadLine();
            if (!Regex.IsMatch(code, @"^[A-Z]{3}$"))
            {
                Logger?.Invoke(string.Format("[Error] Invalid airport code: {0}", code));
                return;
            }

            Console.Write("City: ");
            string city = Console.ReadLine();
            if (city.Length > 50)
            {
                Logger?.Invoke(string.Format("[Error] City length should be 0-50 chracters long: {0}", city));
                return;
            }

            // Add to database
            ctx.AirportCollection.Add(new Airport() { Code = code, City = city });
            ctx.SaveChanges();
            Console.WriteLine("Airport added successfully");
        }

        static void DisplayAirportList()
        {
            Console.WriteLine("-------- Airport List --------");
            foreach (Airport airport in ctx.AirportCollection)
            {
                Console.WriteLine(airport);
            }
            Console.WriteLine("------------------------------");
        }

        static void RegisterFlight()
        {
            Console.Write("Fright Number: ");
            string frightNumber = Console.ReadLine();
            if (!Regex.IsMatch(frightNumber, @"^[A-Za-z]{2}[0-9]{3}$"))
            {
                Logger?.Invoke(string.Format("[Error] Invalid fright number: {0}", frightNumber));
                return;
            }

            // display airport list
            DisplayAirportList();

            Console.Write("Select From Airport Id: ");
            string fromAirportStr = Console.ReadLine();
            if (!int.TryParse(fromAirportStr, out int fromAirportId))
            {
                Logger?.Invoke(string.Format("[Error] Invalid airport id value", fromAirportStr));
                return;
            }

            Console.Write("Select To Airport Id: ");
            string toAirportStr = Console.ReadLine();
            if (!int.TryParse(toAirportStr, out int toAirportId))
            {
                Logger?.Invoke(string.Format("[Error] Invalid airport id value: {0}", toAirportStr));
                return;
            }

            // check fromAirportId and toAirportId is same.
            if (fromAirportId == toAirportId)
            {
                Logger?.Invoke(string.Format("[Warning] FromAirport and ToAirport should not be same", toAirportStr));
                return;
            }

            // check airport id is in database
            Airport fromAirport = ctx.AirportCollection.Where(airport => airport.Id == fromAirportId).SingleOrDefault();
            Airport toAirport = ctx.AirportCollection.Where(airport => airport.Id == toAirportId).SingleOrDefault();
            if (fromAirport == null || toAirport == null)
            {
                Logger?.Invoke(string.Format("[Error] Airport id is not registered in database: {0}", toAirportStr));
                return;
            }

            // Add to database
            ctx.FlightCollection.Add(new Flight() { Number = frightNumber, FromAirport = fromAirport, ToAirport = toAirport });
            ctx.SaveChanges();
            Console.WriteLine("Flight added successfully");
        }

        static void ShowAllFlightsInSpecificAirport()
        {
            // display airport list
            DisplayAirportList();

            Console.Write("Enter Airport Code: ");
            string code = Console.ReadLine();
            if (!Regex.IsMatch(code, @"^[A-Z]{3}$"))
            {
                Logger?.Invoke(string.Format("[Error] Invalid airport code: {0}", code));
                return;
            }

            // This is not working properly because flightlist is not mapping to toAirport and FromAirport
            // flightlist is empty
            Airport selectedAirport = ctx.AirportCollection.Where(airport => airport.Code == code).SingleOrDefault();

            if (selectedAirport == null)
            {
                Logger?.Invoke(string.Format("[Error] Airport code({0}) is not registered in the database", code));
                return;
            }

            // display flight list

            Console.WriteLine(string.Format("\n*** Flight list in Airport {0} ***", selectedAirport));

            List<Flight> flightList = ctx.FlightCollection.Where(flight => flight.FromAirport.Id == selectedAirport.Id || flight.ToAirport.Id == selectedAirport.Id).ToList();
            if (flightList.Count() == 0)
            { 
                Console.WriteLine("No flight in airport");
                return;
            }

            foreach (Flight flight in flightList)
            {
                Console.WriteLine(flight);
            }
            Console.WriteLine();
        }

        static void DeleteAirportAndAllFlightsAssociated()
        {
            // display airport list
            DisplayAirportList();

            Console.Write("Enter Airport Code: ");
            string code = Console.ReadLine();
            if (!Regex.IsMatch(code, @"^[A-Z]{3}$"))
            {
                Logger?.Invoke(string.Format("[Error] Invalid airport code: {0}", code));
                return;
            }

            // select airport
            Airport selectedAirport = ctx.AirportCollection.Where(airport => airport.Code == code).SingleOrDefault();
            if (selectedAirport == null)
            {
                Logger?.Invoke(string.Format("[Error] Airport code({0}) is not registered in the database", code));
                return;
            }

            // get flight list
            List<Flight> flightList = ctx.FlightCollection.Where(flight => flight.FromAirport.Id == selectedAirport.Id || flight.ToAirport.Id == selectedAirport.Id).ToList();

            // delete flight list associated with airport
            foreach (Flight flight in flightList)
            {
                ctx.FlightCollection.Remove(flight);
            }
            ctx.AirportCollection.Remove(selectedAirport);
            ctx.SaveChanges();

            Console.WriteLine("Aiport and Assiociated flight was deleted successfully");
        }

        static void DisplaySuperFibNumber()
        {
            try
            {
                Console.Write("Which super-fib would you like to see? ");
                if (!int.TryParse(Console.ReadLine(), out int nth))
                {
                    Logger?.Invoke("[Error] Invalid airport id value");
                    return;
                }
                Console.WriteLine(string.Format("Super - fib is {0}", superFib[nth]));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Logger?.Invoke(ex.Message);
            }
        }

        static void ChangeLoggerSetting()
        {
            Console.WriteLine("\n1.to console / screen\n2.to file\n3.to database");
            Console.Write("Your choices: ");

            // Get data from user
            string[] newChoiceArr = Console.ReadLine().Split(',');
            if (newChoiceArr[0] == "")
            {
                Logger = null;
                Console.WriteLine("Logger Disabled");
                return;
            }

            Logger = null;
            foreach (string choiceStr in newChoiceArr)
            {
                // FIXME: if user type same number
                if (!int.TryParse(choiceStr, out int choice))
                {
                    Logger?.Invoke("[Error] Invalid logger option");
                    continue;
                }
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Logger enabled: 1 - to console");
                        Logger += LogToScreen;
                        break;
                    case 2:
                        Console.WriteLine("Logger enabled: 2 - to file");
                        Logger += LogToFile;
                        break;
                    case 3:
                        Console.WriteLine("Logger enabled: 3 - to database");
                        Logger += LogToDatabase;
                        break;
                    default:
                        Logger?.Invoke("[Error] Invalid logger option");
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // Set default setting for Logger
                Logger += LogToScreen;
                Logger += LogToFile;
                Logger += LogToDatabase;

                int command;
                do
                {
                    command = GetUserCommand();
                    try
                    {
                        switch (command)
                        {
                            case 0:
                                break;
                            case 1:
                                AddAirport();
                                break;
                            case 2:
                                RegisterFlight();
                                break;
                            case 3:
                                ShowAllFlightsInSpecificAirport();
                                break;
                            case 4:
                                DeleteAirportAndAllFlightsAssociated();
                                break;
                            case 5:
                                DisplaySuperFibNumber();
                                break;
                            case 6:
                                ChangeLoggerSetting();
                                break;
                            default:
                                Console.WriteLine("Invalid command try again.\n");
                                break;
                        }
                    }
                    catch (DataException ex)      // database exception
                    {
                        Logger?.Invoke(string.Format("[Error] {0}", ex.Message));
                    }
                    catch (SystemException ex)    // database exception
                    {
                        Logger?.Invoke(string.Format("[Error] {0}", ex.Message));
                    }
                } while (command != 0);

                Console.WriteLine("\nGood bye!\n");
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
