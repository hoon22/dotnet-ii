﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Flight
    {
        public int Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(5)]
        public string Number { get; set; } // two letters followed by two or three digits, e.g. UA234	

        public virtual Airport FromAirport { get; set; }
        public virtual Airport ToAirport { get; set; }

        public override string ToString()
        {
            return string.Format("{0}. {1} from {2} to {3}", Id, Number, FromAirport.Code, ToAirport.Code);
        }
    }
}
