﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class SuperFib
    {
        List<long> superFibCache = new List<long>();

        public SuperFib ()
        {
            superFibCache.Add(0);
            superFibCache.Add(1);
            superFibCache.Add(2);
        }

        public long this[int index]
        {
            get
            {
                if (index < 0)
                    throw new ArgumentOutOfRangeException("SuperFibs only exist fot non-negative numbers.");
                return GetSuperFibNumber(index);
            }
        }

        long GetSuperFibNumber(int nth)
        {
            if (nth < superFibCache.Count)
                return superFibCache[nth];

            long newSuperFibNumber = GetSuperFibNumber(nth - 1) + GetSuperFibNumber(nth - 2) + GetSuperFibNumber(nth -3);
            superFibCache.Add(newSuperFibNumber);

            return newSuperFibNumber;
        }
    }
}
