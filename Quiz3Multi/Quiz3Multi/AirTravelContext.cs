﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    class AirTravelContext2 : DbContext
    {
        public AirTravelContext2() : base() { }

        public DbSet<Airport> AirportCollection { get; set; }
        public DbSet<Flight> FlightCollection { get; set; }
        public DbSet<LogMessage> LogMsgCollection { get; set; }
    }
}
