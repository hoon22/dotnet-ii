namespace Quiz3Multi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Quiz3Multi.AirTravelContext2>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Quiz3Multi.AirTravelContext";
        }

        protected override void Seed(Quiz3Multi.AirTravelContext2 context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
