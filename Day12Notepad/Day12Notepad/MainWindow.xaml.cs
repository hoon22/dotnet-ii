﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string currentOpenFileName = "";    // no file is open
        string currentOpenFilePath = "";    // path + file name
        bool isDocumentModified = false;

        public MainWindow()
        {
            InitializeComponent();

            UpdateUI();
        }

        void CreateNewFile()
        {
            currentOpenFileName = "";
            currentOpenFilePath = "";
            tbDocument.Text = "";
            isDocumentModified = false;
        }

        void UpdateUI()
        {
            Title = currentOpenFileName == "" ? "Untitled" : currentOpenFileName;
            Title = Title + (isDocumentModified ? " (Modified)" : "");

            tblStatusBar.Text = (currentOpenFilePath == "") ? "No file open" : currentOpenFilePath;
        }

        void SaveTextToFile(string fileName)
        {
            try
            {
                File.WriteAllText(fileName, tbDocument.Text);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Fail to save data: " + ex.Message, "NotepadX", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TbDocument_TextChanged(object sender, TextChangedEventArgs e)
        {
            isDocumentModified = true;
            UpdateUI();
        }

        private void FileNew_MenuClick(object sender, RoutedEventArgs e)
        {
            if (isDocumentModified)
            {
                string msg = string.Format("Do you want to save changes to {0}?", currentOpenFileName ?? "Untitled");
                MessageBoxResult result = MessageBox.Show(msg, "NotepadX", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        // display save dialog
                        FileSave_MenuClick(sender, e);
                        UpdateUI();
                        break;
                    case MessageBoxResult.No:
                        CreateNewFile();
                        UpdateUI();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }
            }
            else
            {
                CreateNewFile();
            }
        }

        private void FileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            if (isDocumentModified)
            {
                string msg = string.Format("Do you want to save changes to {0}?", currentOpenFileName ?? "Untitled");
                MessageBoxResult result = MessageBox.Show(msg, "NotepadX", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        // display save dialog
                        FileSave_MenuClick(sender, e);
                        return;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                    default:
                        MessageBox.Show("Internal error");
                        return;
                }
            }

            OpenFileDialog openFiledlg = new OpenFileDialog();

            openFiledlg.DefaultExt = ".txt";      // Default file extension
            openFiledlg.Filter = "Text files|*.txt|All files|*.*";

            if (openFiledlg.ShowDialog() == true)
            {
                try
                {
                    currentOpenFilePath = openFiledlg.FileName;
                    currentOpenFileName = openFiledlg.SafeFileName;     // save filename only
                    tbDocument.Text = File.ReadAllText(currentOpenFilePath);
                    isDocumentModified = false;

                    UpdateUI();
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Fail to open file: " + ex.Message, "NotepadX", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void FileSave_MenuClick(object sender, RoutedEventArgs e)
        {
            if (currentOpenFilePath == "")
            {
                FileSaveAs_MenuClick(sender, e);
            }
            else
            {
                SaveTextToFile(currentOpenFilePath);
                isDocumentModified = false;
            }
            UpdateUI();
        }

        private void FileSaveAs_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDlg = new SaveFileDialog();

            saveFileDlg.Filter = "Text files|*.txt|All files|*.*";
            saveFileDlg.Title = "Save a file";
            saveFileDlg.ShowDialog();

            if (saveFileDlg.FileName != "")
            {
                SaveTextToFile(saveFileDlg.FileName);
                currentOpenFilePath = saveFileDlg.FileName;
                currentOpenFileName = saveFileDlg.SafeFileName;
                isDocumentModified = false;
            }
            UpdateUI();
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isDocumentModified)
            {
                string msg = string.Format("Do you want to save changes to {0}?", currentOpenFileName ?? "Untitled");
                MessageBoxResult result = MessageBox.Show(msg, "NotepadX", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        // display save dialog
                        FileSave_MenuClick(sender, null);
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }
            }
        }
    }
}
