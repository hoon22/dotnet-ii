﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11TodosInFile
{
    public class TodoItem
    {
        public TodoItem() { }
        public TodoItem(string dataLine)
        {
            // parsing data
            string[] data = dataLine.Split(';');
            if (data.Length != 3)
            {
                throw new InvalidDataException("Invalid Data string format: " + dataLine);
            }

            // get task value
            Task = data[0];

            // convert due date string to DateTime
            // if error, it will throw FormatException
            DueDate = Convert.ToDateTime(data[1]);

            // check status data
            IsDone = data[2] == "done";
        }

        public string Task;
        public DateTime DueDate;
        public bool IsDone;

        public override string ToString()
        {
            return string.Format("{0} by {1} is {2}", Task, DueDate.ToString("d"), (IsDone ? "done" : "pending"));
        }

        public string ToSaveFormat()
        {
            return string.Format("{0};{1};{2}", Task, DueDate.ToString("yyyy-MM-dd"), (IsDone ? "done" : "pending"));
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string DEFAULT_FILENAME = @"todolist.dat";
        List<TodoItem> todoItemList = new List<TodoItem>();

        // method to load file
        void LoadFromFile(string fileName)
        {
            try
            {
                string[] dataList = File.ReadAllLines(fileName);

                foreach (string line in dataList)
                {
                    try
                    {
                        // Add item into the list
                        todoItemList.Add(new TodoItem(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        // FIXME: need to optimize
                        MessageBox.Show(ex.Message, "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                // update list view
                lvTodos.Items.Refresh();
                // update status bar
                tblStatusBar.Text = string.Format("{0} records loaded from {1}", dataList.Length, fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Fail to read file : " + ex.Message, "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Invalid type in Due Date : " + ex.Message, "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        void SaveToFile(string fileName, List<TodoItem> todoList)
        {
            try
            {
                List<string> dataList = new List<string>();
                foreach (TodoItem todo in todoList)
                {
                    dataList.Add(todo.ToSaveFormat());
                }
                File.WriteAllLines(fileName, dataList);
                tblStatusBar.Text = string.Format("{0} records saved to {1}", dataList.Count, fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Fail to save a file " + ex.Message, "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            // data binding
            lvTodos.ItemsSource = todoItemList;

            // check if file exists.
            if (File.Exists(DEFAULT_FILENAME))
            {
                LoadFromFile(DEFAULT_FILENAME);
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveToFile(DEFAULT_FILENAME, todoItemList);
        }

        private void ButtonAddAndUpdateTask_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                if (!Regex.IsMatch(task, @"^[^;]{1,100}$")) {
                    MessageBox.Show("Invalid task input, semicolon is not allowed", "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                DateTime dueDate = dpDueDate.SelectedDate.Value;
                bool isDone = rbStatusDone.IsChecked == true;
                /*
                if (rbStatusPending.IsChecked == true)
                {
                    isDone = false;
                }
                else if (rbStatusDone.IsChecked == true)
                {
                    isDone = true;
                }
                else
                {
                    MessageBox.Show("Internal system error, Status", "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                */
                
                if ((sender as Button).Name == "btUpdateTask")
                {
                    TodoItem item = lvTodos.SelectedItem as TodoItem;
                    item.Task = task;
                    item.DueDate = dueDate;
                    item.IsDone = isDone;

                    tblStatusBar.Text = string.Format("Task '{0}' was updated", task);
                }
                else
                {
                    TodoItem newTodoItem = new TodoItem() { Task = task, DueDate = dueDate, IsDone = isDone };

                    todoItemList.Add(newTodoItem);
                    tblStatusBar.Text = string.Format("Task '{0}' was added into the list", task);

                    // clear the input UI elements
                    tbTask.Text = "";
                    dpDueDate.SelectedDate = null;
                    rbStatusPending.IsChecked = true;

                    lvTodos.SelectedIndex = -1;
                    btUpdateTask.IsEnabled = false;
                    btDeleteTask.IsEnabled = false;
                }
                // update list view
                lvTodos.Items.Refresh();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Please select a Due Date", "Todos in File", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Invalid Due Date value : " + ex.Message, "Todos in File", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvTodos.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select todo items to save", "Todos in File", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            SaveFileDialog saveFileDlg = new SaveFileDialog();

            saveFileDlg.Filter = "Todo list files|*.todos|All files|*.*";
            saveFileDlg.Title = "Save todo items to file";
            saveFileDlg.ShowDialog();

            if (saveFileDlg.FileName != "")
            {
                /*
                List<TodoItem> todoList = new List<TodoItem>();

                foreach (TodoItem todoItem in lvTodos.SelectedItems)
                {
                    todoList.Add(todoItem);
                }
                SaveToFile(saveFileDlg.FileName, todoList);
                */

                // Both work fine.
                SaveToFile(saveFileDlg.FileName, lvTodos.SelectedItems.Cast<TodoItem>().ToList());
                // SaveToFile(saveFileDlg.FileName, lvTodos.SelectedItems.OfType<TodoItem>().ToList());
            }
        }

        private void FileImport_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFiledlg = new OpenFileDialog();

            openFiledlg.FileName = "TodoList";      // Default file name
            openFiledlg.DefaultExt = ".todos";        // Default file extension
            openFiledlg.Filter = "Todo list files|*.todos|All Files|*.*";    // Filter files by extension

            if (openFiledlg.ShowDialog() == true)
            {
                LoadFromFile(openFiledlg.FileName);
            }
        }

        private void SortTask_MenuClick(object sender, RoutedEventArgs e)
        {
            todoItemList = todoItemList.OrderBy(item => item.Task).ToList();
            lvTodos.ItemsSource = todoItemList;
            tblStatusBar.Text = "Sorted todo list by Task";
        }

        private void SortDueDate_MenuClick(object sender, RoutedEventArgs e)
        {
            // todoItemList = todoItemList.OrderBy(item => item.DueDate).ToList();
            var query = from todo in todoItemList orderby todo.DueDate select todo;

            todoItemList = query.ToList();
            lvTodos.ItemsSource = todoItemList;
            tblStatusBar.Text = "Sorted todo list by Due date";
        }

        private void ButtonDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            TodoItem item = lvTodos.SelectedItem as TodoItem;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete:\n" + item, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);

            if (result == MessageBoxResult.OK)
            {
                todoItemList.Remove(item);
                lvTodos.Items.Refresh();
            }
        }

        private void LvTodos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TodoItem item = lvTodos.SelectedItem as TodoItem;

            if (item == null)
            {
                btUpdateTask.IsEnabled = false;
                btDeleteTask.IsEnabled = false;
                return;
            }
            btUpdateTask.IsEnabled = true;
            btDeleteTask.IsEnabled = true;

            // update UI using currently selected item.
            tbTask.Text = item.Task;
            dpDueDate.SelectedDate = item.DueDate;

            rbStatusDone.IsChecked = item.IsDone;
            rbStatusPending.IsChecked = !item.IsDone;

            tblStatusBar.Text = string.Format("Task '{0}' was selected", item.Task);
        }
    }
}
