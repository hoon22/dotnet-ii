﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1Library
{
    public class DataInvalidException : Exception
    {
        public DataInvalidException(String msg) : base(msg) { }
        public DataInvalidException(String msg, Exception ex) : base(msg, ex) { }
    }

    public class Book
    {
        public const int GENRE_COUNT = 4;
        public static string[] GenreList = new string[GENRE_COUNT]
        {
            "Action", "Romans", "Fantasy", "Other"
        };

        public Book() { }
        public Book(string author, string title, string genre, double price, DateTime pubDate)
        {
            InitializeAttribute(author, title, genre, price, pubDate);
        }

        public Book(string dataLine)
        {
            // line data format
            // YUL;Montreal;45.468998124;-73.737830382;36
            string[] data = dataLine.Split(';');

            // check length of parsing data
            if (data.Length != 5)
            {
                throw new DataInvalidException("Invalid data format : " + dataLine);
            }

            string author = data[0];
            if (!Regex.IsMatch(author, @"^[^;]{1,100}$"))
            {
                throw new DataInvalidException("Invalid data: Author should be 1-100 characters long without semicolon");
            }

            string title = data[1];
            if (!Regex.IsMatch(title, @"^[^;]{1,100}$"))
            {
                throw new DataInvalidException("Invalid data: Title should be 1-100 characters long without semicolon");
            }

            string genre = data[2];
            if (!GenreList.Contains(genre))
            {
                throw new DataInvalidException("Invalid data: Genre is not on the genre list");
            }

            double price;
            DateTime pubDate;
            try
            {
                price = double.Parse(data[3]);
                if (price < 0 || 1000 < price)
                {
                    throw new DataInvalidException("Invalid data: Price should be $0 - $1000.");
                }
                pubDate = Convert.ToDateTime(data[4]);
            }
            catch (FormatException ex)
            {
                throw new DataInvalidException("Invalid data: " + dataLine, ex);
            }

            InitializeAttribute(author, title, genre, price, pubDate);
        } // throws DataInvalidException

        public string Author;   // 1-100 characters, no semicolons
        public string Title;    // 1-100 characters, no semicolons
        public string Genre;    // drop-box: Romance, Self-Help, Other
        public double Price;    // 0-10000, display with 2 decimal places
        public DateTime PubDate;    // publication date

        private void InitializeAttribute(string author, string title, string genre, double price, DateTime pubDate)
        {
            Author = author;
            Title = title;
            Genre = genre;
            Price = price;
            PubDate = pubDate;
        }

        public override string ToString()
        {
            return string.Format("{0} by {1} is {2}(${3:0.00}) pub. {4}", Author, Title, Genre, Price, PubDate.ToShortDateString());
        }
        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3:0.00};{4}", Author, Title, Genre, Price, PubDate.ToShortDateString());
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string LIBDATA_FILE_PATH = @"..\..\..\library.dat";
        List<Book> bookList = new List<Book>();

        public MainWindow()
        {
            InitializeComponent();
            // Initialize Genre
            cmbGenre.ItemsSource = Book.GenreList;

            // load data from file
            LoadDataFromFile(LIBDATA_FILE_PATH);

            lvBooks.ItemsSource = bookList;
        }

        void SaveDataToFile(string filePath, List<Book> dataList)
        {
            List<string> saveDataList = new List<string>();
            foreach (Book book in dataList)
            {
                saveDataList.Add(book.ToDataString());
            }
            try
            {
                File.WriteAllLines(filePath, saveDataList);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Fail to save data to file " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void LoadDataFromFile(string filePath)
        {
            try
            {
                string[] dataArray = File.ReadAllLines(filePath);
                foreach (string line in dataArray)
                {
                    try
                    {
                        bookList.Add(new Book(line));
                    }
                    catch (DataInvalidException ex)
                    {
                        // FIXME: correct all of the exception and then display once..
                        MessageBox.Show("[ERROR]: " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                // Update listview
                lvBooks.Items.Refresh();
                // Update status bar
                tblStatusBar.Text = string.Format("{0} records successfully loaded from file.", dataArray.Length);
            }
            catch (FileNotFoundException) { }   // ignore, continue to run program
            catch (IOException ex)
            {
                MessageBox.Show("Fail to read file " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ButtonAddOrUpdate_Click(object sender, RoutedEventArgs e)
        {
            string author = tbAuthor.Text;
            if (!Regex.IsMatch(author, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("Invalid data: Author should be 1-100 characters long without semicolon", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            string title = tbTitle.Text;
            if (!Regex.IsMatch(title, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("Invalid data: Title should be 1-100 characters long without semicolon", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (cmbGenre.SelectedValue == null)
            {
                MessageBox.Show("Please select genre", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            string genre = cmbGenre.SelectedValue.ToString();
            if (!Book.GenreList.Contains(genre))
            {
                MessageBox.Show("Invalid data value: Genre", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!double.TryParse(tbPrice.Text, out double price) || price < 0 || 1000 < price)
            {
                MessageBox.Show("Invalid data value: Price should be 0 - 1000 dollors", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (dpDatePub.SelectedDate == null)
            {
                MessageBox.Show("Please select published date", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DateTime pubDate = dpDatePub.SelectedDate.Value;

            if ((sender as Button).Name == "btUpdate")
            {
                // Update
                Book selectedBook = lvBooks.SelectedValue as Book;

                selectedBook.Author = author;
                selectedBook.Title = title;
                selectedBook.Genre = genre;
                selectedBook.Price = price;
                selectedBook.PubDate = pubDate;

                // update status bar
                tblStatusBar.Text = string.Format("{0} was updated", selectedBook.Title);
            }
            else
            {
                // Add
                try
                {
                    bookList.Add(new Book(author, title, genre, price, pubDate));
                    CleanInput();   // need to call
                    // update status bar
                    tblStatusBar.Text = string.Format("{0} was added", title);
                }
                catch (DataInvalidException ex)
                {
                    // FIXME: correct all of the exception and then display once..
                    MessageBox.Show("[ERROR]: " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            lvBooks.Items.Refresh();

            // call query again
            TbSearch_TextChanged(sender, null);
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDataToFile(LIBDATA_FILE_PATH, bookList);
        }

        void CleanInput()
        {
            tbAuthor.Text = "";
            tbTitle.Text = "";
            cmbGenre.SelectedIndex = -1;
            tbPrice.Text = "0";
            dpDatePub.SelectedDate = null;

            lvBooks.SelectedIndex = -1;     // clear book list view

            // update button
            btUpdate.IsEnabled = false;
            btDelete.IsEnabled = false;
        }

        private void LvBooks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvBooks.SelectedIndex == -1)    // not selected anything
            {
                CleanInput();
                return;
            }
            btUpdate.IsEnabled = true;
            btDelete.IsEnabled = true;

            Book selectedBook = lvBooks.SelectedValue as Book;

            tbAuthor.Text = selectedBook.Author;
            tbTitle.Text = selectedBook.Title;
            cmbGenre.SelectedValue = selectedBook.Genre;
            tbPrice.Text = selectedBook.Price.ToString();
            dpDatePub.SelectedDate = selectedBook.PubDate;

            // update status bar
            tblStatusBar.Text = string.Format("{0} was selected", tbTitle.Text);
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            Book item = lvBooks.SelectedItem as Book;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete:\n" + item, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);

            if (result == MessageBoxResult.OK)
            {
                bookList.Remove(item);
                lvBooks.Items.Refresh();

                // update status bar
                tblStatusBar.Text = string.Format("{0} was deleted", item.Title);

                // call query again
                TbSearch_TextChanged(sender, null);
            }
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvBooks.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select todo items to save", "Todos in File", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            SaveFileDialog saveFileDlg = new SaveFileDialog();

            saveFileDlg.Filter = "Book list files|*.books|All files|*.*";
            saveFileDlg.Title = "Save books to file";
            saveFileDlg.ShowDialog();

            if (saveFileDlg.FileName != "")
            {
                SaveDataToFile(saveFileDlg.FileName, lvBooks.SelectedItems.Cast<Book>().ToList());
                // update status bar
                tblStatusBar.Text = string.Format("{0} books was saved to file", lvBooks.SelectedItems.Count);
            }
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string keyword = tbSearch.Text;
            if (keyword == "")
            {
                lvBooks.ItemsSource = bookList;
                return;
            }

            var query = from book in bookList where book.Author.Contains(keyword) || book.Title.Contains(keyword) || book.Genre.Contains(keyword) select book;

            // FIXME: need more test..
            lvBooks.ItemsSource = query.ToList();  // curDisplayBookList;
        }
    }
}
