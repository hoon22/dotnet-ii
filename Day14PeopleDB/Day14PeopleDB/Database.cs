﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14PeopleDB
{
    public class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Jeonghoon\Documents\labs\DotNet-II\JhDB.mdf;Integrated Security=True;Connect Timeout=30";
        // DB connection
        SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        public List<Person> GetAllPeople(string sortBy)
        {
            List<Person> peopleList = new List<Person>();
            string query = string.Format("SELECT * FROM People ORDER BY {0} ASC", sortBy);
            SqlCommand cmdSelect = new SqlCommand(query, conn);

            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    // int id = reader.GetInt32(0);   // (int)reader[0];
                    // string name = (string)reader[1];
                    // int age = (int)reader[2];
                    // same as above
                    peopleList.Add(new Person() { Id = reader.GetInt32(0), Name = reader.GetString(1), Age = reader.GetInt32(2) });
                }
            }
            return peopleList;
        }

        public int AddPerson(Person person)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People (Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);

            cmdInsert.Parameters.AddWithValue("Name", person.Name);
            cmdInsert.Parameters.AddWithValue("Age", person.Age);

            int insertId = (int)cmdInsert.ExecuteScalar();
            person.Id = insertId;   // we may be able to implement like this.. optional

            return insertId;
        }

        public bool UpdatePerson(Person person)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE People SET Name=@Name, Age=@Age WHERE Id=@Id;", conn);

            cmdUpdate.Parameters.AddWithValue("Id", person.Id);
            cmdUpdate.Parameters.AddWithValue("Name", person.Name);
            cmdUpdate.Parameters.AddWithValue("Age", person.Age);

            // Maybe I would perfer to throw SqlException in case row wa not found?
            // Problem: if row exists but was updated with the same values then
            // affected rows in still 0, so we'd have to execute select to find record first.
            return cmdUpdate.ExecuteNonQuery() > 0 ;
        }

        public bool DeletePerson(int id)
        {
            SqlCommand cmdUpdate = new SqlCommand("DELETE FROM People WHERE Id=@Id;", conn);

            cmdUpdate.Parameters.AddWithValue("Id", id);

            return cmdUpdate.ExecuteNonQuery() > 0;
        }
    }
}
