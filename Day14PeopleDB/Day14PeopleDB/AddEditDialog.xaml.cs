﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day14PeopleDB
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Person EditedPerson;

        public AddEditDialog(Window owner, Person editedPerson = null)
        {
            InitializeComponent();

            Owner = owner;
            EditedPerson = editedPerson;
            if (EditedPerson != null)
            {
                btAddEdit.Content = "Update";
                lblId.Content = EditedPerson.Id;
                tbName.Text = EditedPerson.Name;
                tbAge.Text = EditedPerson.Age.ToString();
            }
        }

        private void ButtonAddEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                string ageStr = tbAge.Text;

                int age = int.Parse(ageStr);
                if (EditedPerson == null)
                {
                    Person person = new Person() { Name = name, Age = age };
                    Globals.Db.AddPerson(person);
                }
                else
                {
                    EditedPerson.Name = name;
                    EditedPerson.Age = age;
                    Globals.Db.UpdatePerson(EditedPerson);
                }
                // set dialog result
                DialogResult = true;
            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                {
                    MessageBox.Show("Age should be positive integer:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                } else if (ex is SqlException)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }
    }
}
