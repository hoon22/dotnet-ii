﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day14PeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> PeopleList = new List<Person>();
 
        public MainWindow()
        {
            InitializeComponent();
            lvPeople.ItemsSource = PeopleList;

            try
            {
                Globals.Db = new Database();
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();    // close the main window, terminate the program
            }
        }

        private void ReloadList(string sortBy = "Id")
        {
            try
            {
                PeopleList.Clear();
                PeopleList.AddRange(Globals.Db.GetAllPeople(sortBy));
                /*
                            List<Person> list = Globals.db.GetAllPeople();
                            PeopleList.Clear();

                            foreach (Person p in list)
                            {
                                PeopleList.Add(p);
                            }
                */
                lvPeople.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddPerson_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDlg = new AddEditDialog(this);

            if (addEditDlg.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void EditItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            LvPeople_MouseDoubleClick(sender, null);    // redirect event
        }

        private void DeleteItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedItem == null) return;

            Person currPerson = lvPeople.SelectedItem as Person;
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" + currPerson, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeletePerson(currPerson.Id);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void LvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvPeople.SelectedItem == null) return;

            AddEditDialog addEditDialog = new AddEditDialog(this, lvPeople.SelectedItem as Person);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void LvPeople_ColumnHeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;

            if (headerClicked == null) return;

            if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
            {
                string selectedHeader = headerClicked.Column.Header as string;

                ReloadList(selectedHeader);
            }
        }
    }
}
