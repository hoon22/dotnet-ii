﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10ScoopSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            ListViewItem selectedItem = lvFlavours.SelectedValue as ListViewItem;

            if (selectedItem == null)
            {
                MessageBox.Show("[ERROR] Please select a flavour", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            // option 1
            lvOrdered.Items.Add(selectedItem.Content);
            
            // option 2
            /*
            ListViewItem dupItem = new ListViewItem();
            dupItem.Content = item.Content;
            lvOrdered.Items.Add(dupItem)
            */
        }

        private void ButtonDeleteScoop_Click(object sender, RoutedEventArgs e)
        {
            if (lvOrdered.SelectedIndex == -1)
            {
                MessageBox.Show("[ERROR] Please select an ordered item", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            lvOrdered.Items.RemoveAt(lvOrdered.SelectedIndex);
        }

        private void ButtonClearAll_Click(object sender, RoutedEventArgs e)
        {
            lvOrdered.Items.Clear();
        }

        private void LvFlavours_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender == null) return;

            lvOrdered.Items.Add(((ListViewItem)sender).Content);
        }
    }
}
