﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12AirportsList
{
    class Airport
    {
        public Airport() { }

        // Constructor to load data from text line
        public Airport(string line)
        {
            // line data format
            // YUL;Montreal;45.468998124;-73.737830382;36
            string[] data = line.Split(';');

            // check length of parsing data
            if (data.Length != 5)
            {
                throw new InvalidDataException("Invalid data format : " + line);
            }

            // data[2]: 45.468998124
            if (!double.TryParse(data[2], out double latitude))
            {
                throw new InvalidDataException("Invalid latitude data : " + line);
            }

            // data[3]: -73.737830382
            if (!double.TryParse(data[3], out double longitude))
            {
                throw new InvalidDataException("Invalid longitude data : " + line);
            }

            // data[4]: 36
            if (!int.TryParse(data[4], out int elevationMeters))
            {
                throw new InvalidDataException("Invalid elevationMeters data : " + line);
            }

            // set values
            Code = data[0];
            City = data[1];
            Latitude = latitude;
            Longitude = longitude;
            ElevationMeters = elevationMeters;
        }

        // other contructor
        public Airport(string code, string city, double latitude, double longitude, int elevationMeters)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
            ElevationMeters = elevationMeters;
        }

        // getters and setters
        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$"))
                {
                    throw new InvalidDataException("[Error] Code should be 3 upper-case letters. : " + value);
                }
                _code = value;
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{1,100}$"))
                {
                    throw new InvalidDataException("City should be 1-100 characters long : " + value);
                }
                _city = value;
            }
        }

        private double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if ((value < -90.0) || (90.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of latitudes should be from -90 to 90. : " + value);
                }
                _latitude = value;
            }
        }

        private double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if ((value < -180.0) || (180.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of longitudes should be from -180 to 180. : " + value);
                }
                _longitude = value;
            }
        }

        public int _elevationMeters;
        public int ElevationMeters
        {
            get { return _elevationMeters; }
            set
            {
                if (value < -1000 || 10000 < value)
                {
                    throw new InvalidDataException("Range of elevation meters should be -1000 ~ 10000. : " + value);
                }
                _elevationMeters = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}:{1} Airport - Latitude: {2}, Longitude: {3}, ElevationMeters: {4}", Code, City, Latitude, Longitude, ElevationMeters);
        }

        // to save data into file
        public string ToSaveFormat()
        {
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string FILE_NAME = @"airport.txt";
        List<Airport> airportsList = new List<Airport>();

        // load data from file
        void LoadDataFromFile(string fileName)
        {
            try
            {
                string[] dataList = File.ReadAllLines(fileName);
                foreach (string line in dataList)
                {
                    try
                    {
                        airportsList.Add(new Airport(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        // FIXME: catch all of the exception and then display once..
                        MessageBox.Show("Data format error " + ex.Message, "Airport List", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                // Update listview
                lvAirport.Items.Refresh();
                if (dataList.Length > 0) { 
                    // Update status bar
                    tblStatusBar.Text = string.Format("{0} records successfully loaded from file.", dataList.Length);
                }
                else
                {
                    tblStatusBar.Text = string.Format("Program started successfully");
                }
            }
            catch (FileNotFoundException) { }   // ignore, continue to run program
            catch (IOException ex)
            {
                MessageBox.Show("Fail to read file " + ex.Message, "Airport List", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        void SaveDataToFile(string fileName, List<Airport> itemList)
        {
            try
            {
                // Create string list to save data
                List<string> saveDataList = new List<string>();
                foreach (Airport airport in itemList)
                {
                    saveDataList.Add(airport.ToSaveFormat());
                }
                // save string list to file
                File.WriteAllLines(fileName, saveDataList);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Fail to save data to file " + ex.Message, "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public MainWindow()
        {

            InitializeComponent();

            // Load data from default file. 
            // Position is important. should be after InitializeComponent()
            LoadDataFromFile(FILE_NAME);

            // connect airport list to listview
            lvAirport.ItemsSource = airportsList;
        }

        // Clear input field and listview
        private void CleanUI()
        {
            tbCode.Text = "";
            tbCity.Text = "";
            tbLongitude.Text = "";
            tbLatitude.Text = "";
            tbElevationMeters.Text = "";

            lvAirport.SelectedIndex = -1;   // clear selected index
        }

        private void ButtonAddAndUpdateAirport_Click(object sender, RoutedEventArgs e)
        {
            string code = tbCode.Text;
            string city = tbCity.Text;
            string latitudeStr = tbLatitude.Text;
            string longitudeStr = tbLongitude.Text;
            string elevationMeterStr = tbElevationMeters.Text;

            if (!double.TryParse(latitudeStr, out double latitude))
            {
                MessageBox.Show("Invalid latitude data type " + latitudeStr, "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!double.TryParse(longitudeStr, out double longitude))
            {
                MessageBox.Show("Invalid longitude data type " + longitudeStr, "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!int.TryParse(elevationMeterStr, out int elevationMeters))
            {
                MessageBox.Show("Invalid elevationMeters data type " + elevationMeterStr, "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Button senderButton = sender as Button;
            // Same as above
            if ((sender as Button).Name == "btUpdate")
            {
                // Update
                if (lvAirport.SelectedItem == null)
                {
                    MessageBox.Show("Please select airport first", "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
 
                Airport selectedAirport = lvAirport.SelectedItem as Airport;

                selectedAirport.Code = code;
                selectedAirport.City = city;
                selectedAirport.Latitude = latitude;
                selectedAirport.Longitude = longitude;
                selectedAirport.ElevationMeters = elevationMeters;

                // update status bar
                tblStatusBar.Text = string.Format("{0} airport updated", city);
            }
            else
            {
                // Add a new airport
                airportsList.Add(new Airport(code, city, latitude, longitude, elevationMeters));
                // update status bar
                tblStatusBar.Text = string.Format("{0} airport added", city);

                // clear UI
                CleanUI();
            }

            // update UI
            lvAirport.Items.Refresh();
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDataToFile(FILE_NAME, airportsList);
        }

        private void LvAirport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // prevent error when Item deleted
            if (lvAirport.SelectedItem == null)
            {
                CleanUI();
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                return;
            }
            btUpdate.IsEnabled = true;
            btDelete.IsEnabled = true;

            Airport selectedAirport = lvAirport.SelectedItem as Airport;

            // Update UI
            tbCode.Text = selectedAirport.Code;
            tbCity.Text = selectedAirport.City;
            tbLatitude.Text = string.Format("{0}", selectedAirport.Latitude);       // using string.Format()
            tbLongitude.Text = selectedAirport.Longitude.ToString();        // using ToString()
            tbElevationMeters.Text = selectedAirport.ElevationMeters.ToString();

            // update status bar
            tblStatusBar.Text = string.Format("{0}({1}) airport was selected", selectedAirport.City, selectedAirport.Code);
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvAirport.SelectedItem == null)
            {
                MessageBox.Show("Please select airport first", "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Airport selectedAirport = lvAirport.SelectedItem as Airport;

            MessageBoxResult result = MessageBox.Show("Are you sure to delete airport:\n" + selectedAirport, "Airport", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);

            if (result == MessageBoxResult.OK) {
                // need to implement dialog
                airportsList.Remove(selectedAirport);

                lvAirport.Items.Refresh();
                // update status bar
                tblStatusBar.Text = string.Format("{0}({1}) airport was deleted", selectedAirport.City, selectedAirport.Code);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            /* for notepad
            switch(MessageBox.Show("Are you sure to exit?", "Airport", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel))
            {
                case MessageBoxResult.Yes:
                    // display save dialog
                    // SaveFileDialog saveDlg = new SaveFileDialog();
                    // saveDlg.ShowDialog() ;
                    break;
                case MessageBoxResult.No:
                    // close program
                    break;
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    break;
                default:
                    MessageBox.Show("Internal error");
                    break;
            }
            */
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvAirport.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select airport items first", "Airport List", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            SaveFileDialog saveFileDlg = new SaveFileDialog();

            saveFileDlg.Filter = "Airport list files|*.lst|All files|*.*";
            saveFileDlg.Title = "Save selected items";
            saveFileDlg.ShowDialog();

            if (saveFileDlg.FileName != "")
            {
                // Both work fine.
                SaveDataToFile(saveFileDlg.FileName, lvAirport.SelectedItems.Cast<Airport>().ToList());
                // SaveToFile(saveFileDlg.FileName, lvAirport.SelectedItems.OfType<Airport>().ToList());
            }
        }

        private void FileImport_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFiledlg = new OpenFileDialog();

            openFiledlg.FileName = "Airports";      // Default file name
            openFiledlg.DefaultExt = ".lst";      // Default file extension
            openFiledlg.Filter = "Airport list files|*.lst|All files|*.*";    // Filter files by extension

            if (openFiledlg.ShowDialog() == true)
            {
                LoadDataFromFile(openFiledlg.FileName);
            }
        }

        private void SortLatitude_MenuClick(object sender, RoutedEventArgs e)
        {
            // descending order
            airportsList = airportsList.OrderByDescending(airport => airport.Latitude).ToList();

            // Using query-like state
            //  var query = from airport in airportList orderby airport.Latitude descending select airport;
            //  airportList = query.ToList();

            lvAirport.ItemsSource = airportsList;            
        }

        private void SortLongitude_MenuClick(object sender, RoutedEventArgs e)
        {
            // ascending order
            airportsList = airportsList.OrderBy(airport => airport.Longitude).ToList();

            lvAirport.ItemsSource = airportsList;
        }
    }
}
