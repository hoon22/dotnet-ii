﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15TodoDB
{
    public class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896357\MyLabs\DotNet-2\Day14FirstDB\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";
        // DB connection
        SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        public List<Todo> GetAllTodos(string sortBy)
        {
            List<Todo> todoList = new List<Todo>();
            string query = string.Format("SELECT * FROM Todos ORDER BY {0} ASC", sortBy);
            SqlCommand cmdSelect = new SqlCommand(query, conn);

            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    todoList.Add(new Todo() {
                        Id = reader.GetInt32(0),
                        Task = reader.GetString(1),
                        DueDate = reader.GetDateTime(2),
                        IsDone = reader.GetByte(3) == 0 ? false : true
                    });
                }
            }
            return todoList;
        }

        public int AddTodo(Todo Todo)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Todos (Task, DueDate, IsDone) OUTPUT INSERTED.ID VALUES (@Task, @DueDate, @IsDone)", conn);

            cmdInsert.Parameters.AddWithValue("Task", Todo.Task);
            cmdInsert.Parameters.AddWithValue("DueDate", Todo.DueDate);
            cmdInsert.Parameters.AddWithValue("IsDone", Todo.IsDone ? 1 : 0);

            int insertId = (int)cmdInsert.ExecuteScalar();
            Todo.Id = insertId;

            return insertId;
        }

        public bool UpdateTodo(Todo Todo)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE Todos SET Task=@Task, DueDate=@DueDate, IsDone=@IsDone WHERE Id=@Id;", conn);

            cmdUpdate.Parameters.AddWithValue("Id", Todo.Id);
            cmdUpdate.Parameters.AddWithValue("Task", Todo.Task);
            cmdUpdate.Parameters.AddWithValue("DueDate", Todo.DueDate);
            cmdUpdate.Parameters.AddWithValue("IsDone", Todo.IsDone ? 1 : 0);

            // Maybe I would perfer to throw SqlException in case row wa not found?
            // Problem: if row exists but was updated with the same values then
            // affected rows in still 0, so we'd have to execute select to find record first.
            return cmdUpdate.ExecuteNonQuery() > 0 ;
        }

        public bool DeleteTodo(int id)
        {
            SqlCommand cmdUpdate = new SqlCommand("DELETE FROM Todos WHERE Id=@Id;", conn);

            cmdUpdate.Parameters.AddWithValue("Id", id);

            return cmdUpdate.ExecuteNonQuery() > 0;
        }
    }
}
