﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day15TodoDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum SortByHeader { Id, Task, DueDate, IsDone }

        SortByHeader SortBy = SortByHeader.Id;

        List<Todo> TodoList = new List<Todo>();

        public MainWindow()
        {
            InitializeComponent();
            lvTodos.ItemsSource = TodoList;

            try
            {
                Globals.Db = new Database();
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message, "Todo Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();    // close the main window, terminate the program
            }
        }

        private void ReloadList()
        {
            try
            {
                TodoList.Clear();
                TodoList.AddRange(Globals.Db.GetAllTodos(SortBy.ToString()));

                lvTodos.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddTask_MenuClick(object sender, RoutedEventArgs e)
        {

            AddEditDialog addEditDlg = new AddEditDialog(this);

            if (addEditDlg.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void EditItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            LvTodos_MouseDoubleClick(sender, null);    // redirect event
        }

        private void DeleteItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            if (lvTodos.SelectedItem == null) return;

            Todo currPerson = lvTodos.SelectedItem as Todo;
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" + currPerson, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeleteTodo(currPerson.Id);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Todo Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void LvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTodos.SelectedItem == null) return;

            AddEditDialog addEditDialog = new AddEditDialog(this, lvTodos.SelectedItem as Todo);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void LvTodos_ColumnHeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;

            if (headerClicked == null) return;

            if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
            {
                string sortBy = headerClicked.Tag as string;
                switch (sortBy)
                {
                    case "Id":
                        SortBy = SortByHeader.Id;
                        break;
                    case "Task":
                        SortBy = SortByHeader.Task;
                        break;
                    case "DueDate":
                        SortBy = SortByHeader.DueDate;
                        break;
                    case "IsDone":
                        SortBy = SortByHeader.IsDone;
                        break;
                    default:
                        MessageBox.Show("Internal Error : Invalid sort value", "Todo Database", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;                  
                }

                ReloadList();
            }
        }
    }
}
