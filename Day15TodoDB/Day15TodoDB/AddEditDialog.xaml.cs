﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day15TodoDB
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Todo EditedTodo;

        public AddEditDialog(Window owner, Todo editedTodo = null)
        {
            InitializeComponent();

            Owner = owner;
            EditedTodo = editedTodo;

            if (EditedTodo != null)
            {
                Title = "Update Task";
                btAddEdit.Content = "Update";
                lblId.Content = EditedTodo.Id;
                tbTask.Text = EditedTodo.Task;
                dpDueDate.SelectedDate = EditedTodo.DueDate;
                rbStatusDone.IsChecked = EditedTodo.IsDone;
            }
        }

        private void ButtonAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                DateTime dueDate = dpDueDate.SelectedDate.Value;
                bool isDone = rbStatusDone.IsChecked == true;

                if (EditedTodo == null)
                {
                    Todo todo = new Todo() { Task = task, DueDate = dueDate, IsDone = isDone };
                    Globals.Db.AddTodo(todo);
                }
                else
                {
                    EditedTodo.Task = task;
                    EditedTodo.DueDate = dueDate;
                    EditedTodo.IsDone = isDone;
                    Globals.Db.UpdateTodo(EditedTodo);
                }
                // set dialog result
                DialogResult = true;
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Todo Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (ex is InvalidOperationException || ex is ArgumentOutOfRangeException)
                {
                    MessageBox.Show("Invalid Due Date value : " + ex.Message, "Todos Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }
    }
}
