﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19FristEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var ctx = new FirstContext())
                {
                    // Equivalent of INSERT
                    var person = new Person() { Name = "Maria", Age = 18 };

                    ctx.People.Add(person);
                    ctx.SaveChanges();
                }
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
