﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19FristEF
{
    public class FirstContext : DbContext
    {
        public FirstContext() : base() { }

        public virtual DbSet<Person> People { get; set; }
    }
}
