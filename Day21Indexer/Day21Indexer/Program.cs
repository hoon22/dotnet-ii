﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21FibIndexer
{
    public class FibStore
    {
        long FibNumber(int nth)
        {
            if (nth == 1 || nth == 2) return 1;

            return FibNumber(nth - 1) + FibNumber(nth - 2);
        }
        public long this[int index]
        {
            get
            {
                if (index < 1)
                    throw new ArgumentException("Invalid Index");
                return FibNumber(index);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FibStore fs = new FibStore();

                Console.WriteLine(fs[1]);
                Console.WriteLine(fs[2]);
                Console.WriteLine(fs[3]);
                Console.WriteLine(fs[4]);
                Console.WriteLine(fs[5]);

                Console.WriteLine(fs[-4]);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
