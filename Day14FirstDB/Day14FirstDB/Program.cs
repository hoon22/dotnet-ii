﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14FirstDB
{
    class Program
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896357\MyLabs\DotNet-2\Day14FirstDB\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";

        static SqlConnection conn;

        static void Main(string[] args)
        {
            try
            {
                conn = new SqlConnection(DbConnectionString);
                conn.Open();

                // insert example
                try
                {
                    Console.Write("Enter person name: ");
                    string name = Console.ReadLine();

                    SqlCommand cmdInsert = new SqlCommand("INSERT INTO People (Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);

                    cmdInsert.Parameters.AddWithValue("Name", name);
                    cmdInsert.Parameters.AddWithValue("Age", new Random().Next(100));
                    //                    cmdInsert.ExecuteNonQuery();
                    int insertId = (int)cmdInsert.ExecuteScalar();
                    Console.WriteLine("Record inserted successfully with id=" + insertId);
                }
                catch (Exception ex)
                {
                    if (ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("[ERROR] executing query: " + ex.Message);
                    }
                    else
                    {
                        throw ex;
                    }
                }

                // select example
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People", conn);
                    using (SqlDataReader reader = cmdSelect.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(0);   // (int)reader[0];
                            string name = (string)reader[1];
                            int age = (int)reader[2];
                            Console.WriteLine("Person({0}): {1} is {2} y/o.", id, name, age);
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("[ERROR] executing query: " + ex.Message);
                    }
                    else
                    {
                        throw ex;
                    }
                }
                /*
                                // update
                                try
                                {
                                    Console.Write("Enter person id: ");
                                    int id = int.Parse(Console.ReadLine());
                                    Console.Write("Enter new name: ");
                                    string name = Console.ReadLine();
                                    Console.Write("Enter new age: ");
                                    int age = int.Parse(Console.ReadLine());

                                    SqlCommand cmdUpdate = new SqlCommand("UPDATE People SET Name=@Name, Age=@Age WHERE Id=@Id;", conn);

                                    cmdUpdate.Parameters.AddWithValue("Id", id);
                                    cmdUpdate.Parameters.AddWithValue("Name", name);
                                    cmdUpdate.Parameters.AddWithValue("Age", age);

                                    Console.WriteLine("{0} records updated successfully", cmdUpdate.ExecuteNonQuery());
                                }
                                catch (Exception ex)
                                {
                                    if (ex is InvalidCastException || ex is SqlException)
                                    {
                                        Console.WriteLine("[ERROR] executing query: " + ex.Message);
                                    } else if (ex is FormatException)
                                    {
                                        Console.WriteLine("[ERROR] Invalid format: " + ex.Message);
                                    }
                                    else
                                    {
                                        throw ex;
                                    }
                                }
                */
                /*
                                // delete
                                try
                                {
                                    Console.Write("Enter person id you want to delete: ");
                                    int id = int.Parse(Console.ReadLine());

                                    SqlCommand cmdUpdate = new SqlCommand("DELETE FROM People WHERE Id=@Id;", conn);

                                    cmdUpdate.Parameters.AddWithValue("Id", id);
                                    Console.WriteLine("{0} records deleted successfully", cmdUpdate.ExecuteNonQuery());
                                }
                                catch (Exception ex)
                                {
                                    if (ex is InvalidCastException || ex is SqlException)
                                    {
                                        Console.WriteLine("[ERROR] executing query: " + ex.Message);
                                    }
                                    else if (ex is FormatException)
                                    {
                                        Console.WriteLine("[ERROR] Invalid format: " + ex.Message);
                                    }
                                    else
                                    {
                                        throw ex;
                                    }
                                }
                */
            }
            catch(Exception ex)
            {
                if (ex is InvalidOperationException || ex is SqlException)
                {
                    Console.WriteLine("[ERROR] Connecting to database: " + ex.Message);
                }
                else
                {   // MUST throw exception we didn't handle
                    throw ex;
                }
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
